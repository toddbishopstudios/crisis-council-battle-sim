package eu4.comsim.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.swt.SWTResourceManager;

/**
 * Main class that houses the Shell and the main() method.
 */
public class CombatSimulator {
	
	public static final File PERSIST_FILE = new File("lastRun.cs");
	
	private static final String VERSION = "1.2";
	private static final String APPLICATION_ICON = "/general/EU4_icon.png";
	private static final int OPTIMAL_HEIGHT = 850;
	private static final int OPTIMAL_WIDTH = 1000;
	private static final String HELP_MESSAGE = "Version " + VERSION
			+ " (beta). You can help improve this program by submitting comments, feature requests and bugs at https://bitbucket.org/Hottemax/eu4-combatsimulator/issues";
	
	static Shell shlEuCombatSimulator;
	
	/**
	 * Launch the application.
	 *
	 * @param args
	 *            None
	 */
	public static void main(String[] args) {
		shlEuCombatSimulator = createShell();
		MainComposite mainComposite = new MainComposite(shlEuCombatSimulator, SWT.NONE);
		mainComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		try {
			Display display = Display.getDefault();
			shlEuCombatSimulator.open();
			shlEuCombatSimulator.layout();
			while (!shlEuCombatSimulator.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			MessageBox messageBox = new MessageBox(new Shell(Display.getDefault()), SWT.ICON_ERROR);
			messageBox.setMessage("Unexpected error:" + e.toString()
					+ "An error.log has been generated. You can help improve this program by submitting bugs at https://bitbucket.org/Hottemax/eu4-combatsimulator/issues");
			
			messageBox.open();
			File errorLog = new File("error.log");
			try (PrintStream ps = new PrintStream(errorLog);) {
				e.printStackTrace(ps);
			} catch (FileNotFoundException e1) {
				// IGNORE
			}
		}
		
	}
	
	/**
	 * Create contents of the window.
	 */
	private static Shell createShell() {
		shlEuCombatSimulator = new Shell();
		shlEuCombatSimulator.setImage(SWTResourceManager.getImage(MainComposite.class, APPLICATION_ICON));
		shlEuCombatSimulator.setSize(OPTIMAL_WIDTH, OPTIMAL_HEIGHT);
		shlEuCombatSimulator.setMinimumSize(OPTIMAL_WIDTH, OPTIMAL_HEIGHT);
		shlEuCombatSimulator.setText("EU4 Combat Simulator " + VERSION);
		shlEuCombatSimulator.setLocation(100, 50);
		GridLayout gl_shlEuCombatSimulator = new GridLayout(1, false);
		shlEuCombatSimulator.setLayout(gl_shlEuCombatSimulator);
		
		Menu menu = new Menu(shlEuCombatSimulator, SWT.BAR);
		shlEuCombatSimulator.setMenuBar(menu);
		MenuItem mntmHelp = new MenuItem(menu, SWT.CASCADE);
		mntmHelp.setText("Help");
		Menu menu_cascade = new Menu(mntmHelp);
		mntmHelp.setMenu(menu_cascade);
		MenuItem mntmAbout = new MenuItem(menu_cascade, SWT.NONE);
		mntmAbout.setText("About");
		mntmAbout.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				MessageBox messageBox = new MessageBox(shlEuCombatSimulator, SWT.ICON_INFORMATION);
				messageBox.setMessage(HELP_MESSAGE);
				messageBox.open();
			}
		});
		
		return shlEuCombatSimulator;
	}
	
}
