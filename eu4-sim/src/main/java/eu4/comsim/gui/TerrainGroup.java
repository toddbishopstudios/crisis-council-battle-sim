package eu4.comsim.gui;

import java.util.stream.Stream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.wb.swt.SWTResourceManager;

import eu4.comsim.core.datatypes.Terrain;
import eu4.comsim.core.datatypes.Terrain.CrossingPenalty;

/**
 * Contains UI elements regarding choice of {@link Terrain} and potential attacker penalties from it (strait, river,
 * landing ...)
 *
 */
public class TerrainGroup extends Group {
	
	private Combo combo_attackerPenalty;
	
	public static final int imageWidth = 175;
	public static final int imageHeight = 50;
	Terrain selectedTerrain;
	
	/**
	 * Create the composite.
	 *
	 * @param parent
	 * @param style
	 */
	public TerrainGroup(Composite parent, int style, Terrain terrain, CrossingPenalty cp) {
		super(parent, style);
		selectedTerrain = terrain;
		setText("Terrain");
		setLayout(new GridLayout(1, false));
		Button button_terrain = new Button(this, SWT.PUSH);
		GridData gd_button_terrain = new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1);
		gd_button_terrain.heightHint = 100;
		gd_button_terrain.widthHint = 200;
		button_terrain.setLayoutData(gd_button_terrain);
		button_terrain.setToolTipText("Click to choose terrain");
		Image i = SWTResourceManager.getImage(TerrainGroup.class, terrain.imagePath);
		i = new Image(Display.getDefault(), i.getImageData().scaledTo(imageWidth, imageHeight));
		button_terrain.setImage(i);
		
		Menu menu_chooseTerrain = new Menu(button_terrain);
		button_terrain.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Rectangle bounds = button_terrain.getBounds();
				Point point = button_terrain.getParent().toDisplay(bounds.x, bounds.y + bounds.height);
				menu_chooseTerrain.setLocation(point);
				menu_chooseTerrain.setVisible(true);
			}
		});
		for (Terrain t : Terrain.values()) {
			MenuItem item = new MenuItem(menu_chooseTerrain, SWT.RADIO);
			item.setText(t.name);
			item.setSelection(t == selectedTerrain);
			item.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					Image tImg = SWTResourceManager.getImage(TerrainGroup.class,
							Terrain.fromString(item.getText()).imagePath);
					tImg = new Image(Display.getDefault(), tImg.getImageData().scaledTo(imageWidth, imageHeight));
					button_terrain.setImage(tImg);
					selectedTerrain = Terrain.fromString(item.getText());
				}
			});
		}
		combo_attackerPenalty = new Combo(this, SWT.READ_ONLY);
		combo_attackerPenalty
				.setItems(Stream.of(Terrain.CrossingPenalty.values()).map(t -> t.name).toArray(String[]::new));
		combo_attackerPenalty.select(cp.ordinal());
		combo_attackerPenalty.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
	}
	
	public Terrain.CrossingPenalty getPenalty() {
		return Terrain.CrossingPenalty.values()[combo_attackerPenalty.getSelectionIndex()];
	}
	
	public Terrain getTerrain() {
		return selectedTerrain;
	}
	
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
