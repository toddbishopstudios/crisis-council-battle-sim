package eu4.comsim.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import eu4.comsim.core.datatypes.CombatType;
import eu4.comsim.core.datatypes.Phase;
import eu4.comsim.core.datatypes.Unit;
import eu4.comsim.core.datatypes.UnitType;

/**
 * An individual regiment in an {@link Army} or {@link Battleline}
 *
 */
public class Regiment implements Serializable {
	
	private static final long serialVersionUID = -1729390359322467857L;
	
	/** Base strength of a regiment */
	public static final int FULL_STRENGTH = 1000;
	
	/**
	 * Static identifier for an "empty" regiment, signifying the corresponding place in the battlefield is empty
	 */
	public static final Regiment EMPTY = new Regiment(-1, Unit.HALBERD_INFANTRY, 0, new Technology(1));
	
	/**
	 * From observing real game logs, it appears that there is an "infA morale bonus", which only applies during
	 * individual attacks though - it is a flat modifier and does not affect "real" morale which is checks whether a
	 * regiment is routed etc.
	 */
	private static final double ATTACK_MORALE_BONUS = 0.5;
	private int strength;
	private double currentMorale;
	
	private int bufferedCasualties;
	private double bufferedMorale;
	
	public final Unit unit;
	Technology tech;
	
	// Maybe use name and id (not needed for now)
	private int id;
	
	/**
	 * A new regiment is created out of thin air.
	 *
	 * @param id
	 *            Optional identifier.
	 * @param unit
	 *            Unit of the regiment.
	 * @param strength
	 *            Number of individual soldiers, in almost all cases use {@value #FULL_STRENGTH}
	 * @param tech
	 *            Technology of the regiments nation, influences morale, flanking range, damage etc.
	 */
	public Regiment(int id, Unit unit, int strength, Technology tech) {
		this.id = id;
		this.unit = unit;
		this.strength = strength;
		this.tech = tech;
		this.currentMorale = tech.getMorale();
		bufferedCasualties = 0;
		bufferedMorale = 0;
	}
	
	public Regiment() {
		this(-1, Unit.HALBERD_INFANTRY, 0, new Technology(1));
	}
	
	/**
	 * Attack of individual regiment against opponent regiment
	 */
	public void attack(Regiment opponent, Phase phase, int baseRoll, Regiment support, double commonModifier,
			boolean flanking) {
		// TODO Determine if defending artillery defensive pip contribution is
		// rounded to nearest int?
		int supportingDefensivePips = support.isOfType(UnitType.ARTILLERY)
				? support.unit.getPips(phase, CombatType.DEFENSE) / 2 : 0;
		int damageRoll = Math.max(0, baseRoll + unit.getPips(phase, CombatType.OFFENSE)
				- opponent.unit.getPips(phase, CombatType.DEFENSE) - supportingDefensivePips);
		int moraleDamageRoll = Math.max(0, baseRoll + unit.getPips(Phase.MORALE, CombatType.OFFENSE)
				- opponent.unit.getPips(Phase.MORALE, CombatType.DEFENSE));
		int casualties = (int) (baseCasualties(damageRoll) * strength / 1000.0 * commonModifier);
		double moraleDamage = baseCasualties(moraleDamageRoll) / 600.0 * (tech.getMorale() + ATTACK_MORALE_BONUS)
				* commonModifier;
		opponent.storeCasualties(casualties);
		opponent.storeMorale(moraleDamage);
		// Are the supporting def pips counted in this case, though?
		// Regiment in back takes front row unit's morale damage, too (if not
		// flanking)
		if (!flanking) {
			support.storeMorale(moraleDamage);
		}
	}
	
	/**
	 * Changes internal state by applying all buffered changes from {@link #storeCasualties(int)} and
	 * {@link #storeMorale(double)}
	 */
	public void applyDamage() {
		strength = Math.max(0, strength - bufferedCasualties);
		currentMorale = currentMorale - bufferedMorale;
		bufferedMorale = 0;
		bufferedCasualties = 0;
	}
	
	/**
	 * @return Current number of warm bodies in the regiment
	 */
	public int getStrength() {
		return strength;
	}
	
	/**
	 * @return Current morale
	 */
	public double getCurrentMorale() {
		return (strength == 0) ? 0 : Double.max(0, currentMorale);
	}
	
	/**
	 * @return <code>true</code> if morale is above <code>0</code>, <code>false</code> otherwise
	 */
	public boolean routed() {
		return !(getCurrentMorale() > 0);
	}
	
	/**
	 * Changes morale. <br>
	 *
	 * Used for example
	 * <li>(+0.5) at the start of a battle for the infA
	 * <li>(-0.01) after each day regardless if casualties were taken
	 *
	 *
	 * @param value
	 *            The value to change.
	 */
	public void changeMorale(double value) {
		currentMorale += value;
	}
	
	private static double baseCasualties(int roll) {
		return (15 + 5 * roll);
	}
	
	private void storeCasualties(int casualties) {
		bufferedCasualties += casualties;
	}
	
	private void storeMorale(double morale) {
		bufferedMorale += morale;
	}
	
	/**
	 * @return Flanking range of the regiment, depends on its current {@link #strength}
	 */
	public int getFlankingRange() {
		double flankingMod = 1.0;
		if (strength < 750) {
			flankingMod = 0.75;
		}
		if (strength < 500) {
			flankingMod = 0.5;
		}
		if (strength < 250) {
			flankingMod = 0.25;
		}
		return (int) (unit.type.flankingRange * tech.getLevel().flankingRange * flankingMod);
	}
	
	/**
	 * Sets strength to 0 (happens specifically when morale reaches 0 before day 12). Opposite of {@link #reset()}.
	 *
	 * @see #reset()
	 */
	public void wipe() {
		strength = 0;
		currentMorale = 0;
	}
	
	/**
	 * Resets strength and morale to default (pre-Battle) values. Opposite of {@link #wipe()}.
	 *
	 * @see #wipe()
	 */
	public void reset() {
		strength = FULL_STRENGTH;
		currentMorale = tech.getMorale();
	}
	
	/**
	 * Useful for saving a regiment for stats
	 *
	 * @return Regiment, with all values reset (morale, strength, ...)
	 */
	public Regiment copy() {
		Regiment r = new Regiment(id, unit, strength, tech);
		r.currentMorale = currentMorale;
		return r;
	}
	
	/**
	 * Check unit type
	 */
	public boolean isOfType(UnitType type) {
		return (unit.type == type);
	}
	
	/**
	 * Utility method to filter regiments of given type from a source collection.
	 */
	public static Collection<Regiment> getRegiments(Collection<Regiment> regiments, UnitType type) {
		return regiments.stream().filter(r -> r.isOfType(type)).collect(Collectors.toList());
	}
	
	@Override
	public String toString() {
		if (this == Regiment.EMPTY) { return "No regiment"; }
		StringBuilder sb = new StringBuilder();
		sb.append("Type: \t" + unit.name + "\n");
		sb.append("Men: \t " + strength + "\n");
		sb.append("Morale: \t " + Util.DF.format(currentMorale));
		
		return sb.toString();
	}
	
	/**
	 * Creates full-strength regiments with the given self-explanatory parameters.
	 */
	public static final List<Regiment> createRegiments(Technology tech, Map<Unit, Integer> ratio) {
		List<Regiment> regiments = new ArrayList<Regiment>();
		for (Unit unit : ratio.keySet()) {
			for (int i = 0; i < ratio.get(unit); i++) {
				regiments.add(new Regiment(regiments.size(), unit, FULL_STRENGTH, tech));
			}
		}
		return regiments;
	}
	
	/**
	 * Predicate checking for EMPTY regiment (the constant representing empty battlefield positions, not actual
	 * regiments whose strength has been reduced to zero.
	 */
	public static final Predicate<Regiment> IS_EMPTY = (r -> r == Regiment.EMPTY);
	
}
