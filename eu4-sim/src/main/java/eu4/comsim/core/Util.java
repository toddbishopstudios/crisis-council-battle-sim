package eu4.comsim.core;

import java.text.NumberFormat;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import eu4.comsim.core.datatypes.Unit;

public class Util {
	public static final NumberFormat DF = NumberFormat.getInstance();
	static {
		DF.setMaximumFractionDigits(2);
		DF.setMinimumFractionDigits(2);
	}
	
	public static final NumberFormat PF = NumberFormat.getPercentInstance();
	public static NumberFormat IF = NumberFormat.getIntegerInstance();
	
	public static List<Regiment> getRegiments(Technology tech, int inf, int cav, int art) {
		
		@SuppressWarnings("serial")
		Map<Unit, Integer> boilerPlate = new Hashtable<Unit, Integer>() {
			{
				put(Unit.DUTCH_MAURICIAN, inf);
				put(Unit.FRENCH_CARACOLLE, cav);
				put(Unit.CHAMBERED_DEMI_CANNON, art);
				
			}
		};
		return Regiment.createRegiments(tech, boilerPlate);
	}
	
}
