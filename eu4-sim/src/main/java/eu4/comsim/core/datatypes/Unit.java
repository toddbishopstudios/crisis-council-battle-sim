package eu4.comsim.core.datatypes;

import java.util.stream.Stream;

import eu4.comsim.core.Technology;
import eu4.comsim.gui.TechnologyGroup;

/**
 * Enumeration of all Units unlockable in the game.<br>
 *
 * @see <a href="http://www.eu4wiki.com/Land_units">http://www.eu4wiki.com/ Land_units</a>
 */
public enum Unit {
	
	FLYING_BATTERY(29, Technology.Group.WESTERN, UnitType.ARTILLERY, "Flying Battery", 4, 4, 2, 2, 4, 4),
	NAPOLEONIC_LANCERS(28, Technology.Group.WESTERN, UnitType.CAVALRY, "Latin Lancers", 0, 2, 6, 4, 5, 4),
	NAPOLEONIC_SQUARE(30, Technology.Group.WESTERN, UnitType.INFANTRY, "Napoleonic Square", 4, 4, 4, 3, 4, 3),
	
	/**
	 * Military technology level the unit becomes available (in the range of 0-32)
	 */
	public final int tech_level;
	/**
	 * {@link TechnologyGroup} of the Unit. All artillery will be considered WESTERN tech by convention (it is available
	 * to all tech groups).
	 */
	public final Technology.Group tech_group;
	/** Type of the Unit, one of INFANTRY, CAVALRY, ARTILLERY */
	public final UnitType type;
	/** Name of the unit, such as "Western Longbow" */
	public final String name;
	/** Offensive fire pips (0-6) */
	public final int off_fire;
	/** Defensive fire pips (0-6) */
	public final int def_fire;
	/** Offensive shock pips (0-6) */
	public final int off_shock;
	/** Defensive shock pips (0-6) */
	public final int def_shock;
	/** Offensive morale pips (0-6) */
	public final int off_morale;
	/** Defensive morale pips (0-6) */
	public final int def_morale;
	
	private Unit(int tech_level, Technology.Group techgroup, UnitType type, String name, int off_fire, int def_fire,
			int off_shock, int def_shock, int off_morale, int def_morale) {
		this.tech_level = tech_level;
		this.tech_group = techgroup;
		this.type = type;
		this.name = name;
		this.off_fire = off_fire;
		this.def_fire = def_fire;
		this.off_shock = off_shock;
		this.def_shock = def_shock;
		this.off_morale = off_morale;
		this.def_morale = def_morale;
	}
	
	/**
	 * @return Pips of the unit for the given phase (FIRE/SHOCK/MORALE) and combat type (OFFENSE/DEFENSE)
	 */
	public int getPips(Phase phase, CombatType combatType) {
		if (combatType == CombatType.OFFENSE && phase == Phase.FIRE) { return off_fire; }
		if (combatType == CombatType.DEFENSE && phase == Phase.FIRE) { return def_fire; }
		if (combatType == CombatType.OFFENSE && phase == Phase.SHOCK) { return off_shock; }
		if (combatType == CombatType.DEFENSE && phase == Phase.SHOCK) { return def_shock; }
		if (combatType == CombatType.OFFENSE && phase == Phase.FIRE) { return off_fire; }
		if (combatType == CombatType.DEFENSE && phase == Phase.FIRE) { return def_fire; }
		if (combatType == CombatType.OFFENSE && phase == Phase.MORALE) { return off_morale; }
		if (combatType == CombatType.DEFENSE && phase == Phase.MORALE) { return def_morale; }
		return 0;
	}
	
	/**
	 * Convenience method to find a Unit of the given name. If no unit of the exact same name exists, <code>null</code>
	 * is returned
	 */
	public static Unit fromString(String name) {
		return Stream.of(Unit.values()).filter(u -> u.name.equalsIgnoreCase(name)).findAny().orElse(null);
	}
	
	/**
	 * @return The {@link #name} (don't confuse with {@link #name()} from {@link Enum}!
	 */
	public String getName() {
		return name;
	}
	
	public String getTooltip() {
		StringBuilder sb = new StringBuilder();
		// sb.append("(Pips: (off. fire,def. fire,off. shock, def. shock, off.
		// morale, def. morale): \n");
		sb.append("Off. Fire: \t" + off_fire + "\n");
		sb.append("Def. Fire: \t" + def_fire + "\n");
		sb.append("Off. Shock: \t" + off_shock + "\n");
		sb.append("Def. Shock: \t" + def_shock + "\n");
		sb.append("Off. Morale: \t" + off_morale + "\n");
		sb.append("Def. Morale: \t" + def_morale + "\n");
		sb.append("Total Pips: \t"
				+ Integer.toString(off_fire + def_fire + off_shock + def_shock + off_morale + def_morale) + "\n");
		
		return sb.toString();
	}
}