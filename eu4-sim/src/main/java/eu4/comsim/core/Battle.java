package eu4.comsim.core;

import eu4.comsim.core.datatypes.Phase;
import eu4.comsim.core.datatypes.Terrain;
import eu4.comsim.core.datatypes.Terrain.CrossingPenalty;

/**
 * A Battle between two Armies.
 * <p>
 * Usage:</br>
 * <code>Battle b = new Battle(attacker, defender,
 * terrain, crossingPenalty)<br> b.run() <br>b.getStats()</code>
 *
 */
public class Battle implements Runnable {
	
	// TODO Need outside confirmation for these three crucial values!
	private static final double ATTACKER_MORALE_BONUS = 0.0;
	private static final double DAILY_MORALE_CHANGE = -0.015;
	private static final int EARLIEST_RETREAT = 12;
	
	public final Army armyA;
	public final Army armyB;
	
	private int randomRollA, randomRollB;
	
	public Battleline battleLineA;
	public Battleline battleLineB;
	
	public final Terrain terrain;
	public final CrossingPenalty crossingPenalty;
	int day;
	public final int width;
	
	/**
	 * Sets up a battle between two armies. Respective {@link Battleline}s are created and initialized.
	 */
	public Battle(Army attacker, Army defender, Terrain terrain, CrossingPenalty crossingPenalty) {
		armyA = attacker;
		armyB = defender;
		// Effective combat width is the maximum of the individual widths
		// according to wiki
		width = Math.max(attacker.technology.getLevel().combatWidth, defender.technology.getLevel().combatWidth);
		
		battleLineA = new Battleline(attacker.regiments, width, attacker.technology.getGroup().maxCavalryRatio,
				getShockMultiplier(attacker, terrain));
		// Attacker morale bonus, likely to be only during attack
		armyA.regiments.forEach(r -> r.changeMorale(ATTACKER_MORALE_BONUS));
		battleLineB = new Battleline(defender.regiments, width, defender.technology.getGroup().maxCavalryRatio,
				getShockMultiplier(defender, terrain));
		
		this.terrain = terrain;
		this.crossingPenalty = crossingPenalty;
	}
	
	/**
	 * Executes a battle <br>
	 * TODO Refactor for more fine-grained control (i.e. step(), done()) to allow simulation of individual days <br>
	 * TODO Refactor to avoid doubled code (the same mirrored actions for both sides ...)
	 *
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		while (!armyA.defeated() && !armyB.defeated()) {
			simulateDay();
			// TODO Fix "draws" (atm, with very equal starting armies and no
			// penalties, there may be "draws" since we apply this check only
			// after we apply daily morale damage etc.
		}
	}
	
	public void simulateDay() {
		day++;
		if (day % 3 == 1) {
			randomRollA = armyA.dieRollProvider.getNextRoll();
			randomRollB = armyB.dieRollProvider.getNextRoll();
		}
		Phase phase = getPhase(day);
		int environmentPenalty = terrain.attackerPenalty;
		environmentPenalty += (armyA.general.maneuver <= armyB.general.maneuver) ? crossingPenalty.penalty : 0;
		int rollA = randomRollA + armyA.general.difference(armyB.general, phase, environmentPenalty);
		int rollB = randomRollB + armyB.general.difference(armyA.general, phase, 0);
		
		battleLineA.attackAll(battleLineB, phase, rollA);
		battleLineB.attackAll(battleLineA, phase, rollB);
		// Damage is not applied instantly but only if all casualties
		// have been computed in the day (using the original regiment
		// strengths)
		
		armyA.applyBufferedDamage();
		armyB.applyBufferedDamage();
		
		armyA.regiments.forEach(r -> r.changeMorale(DAILY_MORALE_CHANGE));
		armyB.regiments.forEach(r -> r.changeMorale(DAILY_MORALE_CHANGE));
		
		// game doesnt' change regiment position or allow retreats for 12
		// days
		if (day < EARLIEST_RETREAT) {
			// Kill all regiments with 0 morale
			armyA.regiments.stream().filter(Regiment::routed).forEach(Regiment::wipe);
			armyB.regiments.stream().filter(Regiment::routed).forEach(Regiment::wipe);
		} else {
			battleLineA.consolidate();
			battleLineB.consolidate();
		}
		
		// printState();
	}
	
	/**
	 * days (0,1,2) -> FIRE<br>
	 * days (3,4,5) -> SHOCK<br>
	 * days (6,7,8) -> FIRE<br>
	 * ...
	 *
	 * @return Phase corresponding to day
	 */
	private static Phase getPhase(int day) {
		// phase is a six day cycle.
		return (((day - 1) % 6) / 3 == 0) ? Phase.FIRE : Phase.SHOCK;
	}
	
	private static double getShockMultiplier(Army army, Terrain terrain) {
		return (army.technology.getGroup() != Technology.Group.NOMAD) ? 1
				: (terrain.attackerPenalty == 0) ? 1.25 : 0.75;
	}
	
	/**
	 * @return Duration of the battle
	 */
	public int getDuration() {
		return day;
	}
	
	/**
	 * Ignore this
	 */
	@SuppressWarnings("unused")
	private void printState() {
		for (int i = 0; i < battleLineA.frontRow.size() + 1; i++) {
			System.out.print("DDDDDDDD");
		}
		System.out.println();
		
		battleLineA.print();
		for (int i = 0; i < battleLineA.frontRow.size() + 1; i++) {
			System.out.print("--------");
		}
		System.out.println();
		battleLineB.print();
	}
	
}
