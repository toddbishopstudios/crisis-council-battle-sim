## EU4 Combat Simulator ##
* Combat simulator for [Europa Universalis IV](http://www.europauniversalis4.com) by Paradox Interactive
* Version 1.2 (alpha), reflects the state of the game at [Patch 1.18](http://www.eu4wiki.com/Patch_1.18). Accuracy for later versions not guaranteed.

* Not actively maintained at the moment due to other projects, feel free to fork/do merge requests/use the code base in any way you wish. **You can also contact me to get full write access for the repo, if you want to assume the role of the active maintainer/developer.**
* For the same reason, I am sorry to say that I can not actually work on new feature requests
* For the major points of future work possible on this tool according to my opinion, check the [filtered Issues](https://bitbucket.org/Hottemax/eu4-combatsimulator/issues?kind=task&kind=proposal&kind=enhancement&priority=major&priority=blocker&priority=critical&status=open&status=new).

### How do I get it? ###

* Go to [Downloads](https://bitbucket.org/Hottemax/eu4-combatsimulator/downloads) (in the sidebar) and get the correct jar for your platform; the 32bit/64bit distinction pertains to your JVM, not your OS. The download counter does not represent the actual traffic, since it is reset on each new build which triggers redeployment of jars to the Downloads folder. The drone.io service, where it was previously hosted, was discontinued.
* Alternative: manually build it with maven, for example *mvn package -P windows64*
* *IMPORTANT note for Mac users:* macos swt jars need to be manually started with the *-XstartOnFirstThread* command line option, e.g.: *"java -XstartOnFirstThread -jar eu4-combatsimulator-1.2-mac64.jar"*

### Resources used for combat logic modeling

1. [EU4 Wiki: Land warfare](http://www.eu4wiki.com/Land_warfare) (main resource)
2. [EU4 Wiki: Land units](http://www.eu4wiki.com/Land_units)
3. [EU4 Wiki: Military technology](http://www.eu4wiki.com/Technology#Cumulative_mil_tech_effects_to_army)
4. [EU4 Wiki: Technology groups](http://www.eu4wiki.com/Technology#Groups)
5. Knowledge of contributors, [discussions under Issues](https://bitbucket.org/Hottemax/eu4-combatsimulator/issues)

### How to contribute ###
Don't be afraid to hop in and contribute - everyone can help:

* Contribute code via git
* Ideas for new features
* Report bugs
* Point out wrongly modeled combat logic
* Code review

![EU4-CombatSimulator-1.2-GUI.png](https://bitbucket.org/repo/6GxEpA/images/543166591-EU4-CombatSimulator-1.2-GUI.png)